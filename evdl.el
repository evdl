;;; evdl.el -- Emacs Video Downloader
;;;            A web video downloader script for Emacs

;; Copyright (C) 2009 Martin Bealby
;;
;; Author: Martin Bealby <mbealby@gmail.com>
;; 
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;     
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;     
;; You should have received a copy of the GNU General Public
;; License along with this program; if not, write to the Free
;; Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
;; MA 02111-1307, USA.

;;; Notes:
;;   This relies upon emacs-wget to do status feedback on downloads.  It is
;;   available from: http://pop-club.hp.infoseek.co.jp/emacs/emacs-wget/

;;; Usage:
;;   1. M-x evdl and supply the video page url (if you want the url only,
;;   prefix the call)
;;   2. ???
;;   3. Profit!

;;; Todo:
;;   1. Different sites can probably be simplified to a list.  Each entry
;;      being a list with the function to call and its arguments.
;;   2. More sites


;;----------------------------------------------------------------------------
;; requires
;;----------------------------------------------------------------------------
(require 'wget)


;;----------------------------------------------------------------------------
;; group
;;----------------------------------------------------------------------------
(defgroup evdl nil
  "Emacs Video Downloader."
  :prefix "evdl-"
  :group 'evdl)


;;----------------------------------------------------------------------------
;; variables
;;----------------------------------------------------------------------------
;; none


;;----------------------------------------------------------------------------
;; functions
;;----------------------------------------------------------------------------
(defun evdl (url &optional arg)
  "Downloads a flash video from the given url.  If called with a
prefix, display direct video link and append to the kill ring."
  (interactive "sURL: \nP")
  (if arg
	  (setq f '(lambda (p)
				(progn (kill-new p)
					   (message p))))
	  (setq f 'wget)) 
  (cond
	;; youtube (defaults to mp4)
	((string-match "youtube.[A-Za-z]+/watch" url)
	 (funcall f
			  (evdl-extract-link-youtube
			   (substring url
						  (progn (string-match "youtube.[A-Za-z]+/watch.v="
											   url)
								 (match-end 0))
						  (progn (string-match
								  "youtube.[A-Za-z]+/watch.v=[-A-Z_a-z0-9]+"
								  url)
								 (match-end 0))))))
	;; vimeo 
	((string-match "vimeo.com/" url)
	 (funcall f
			  (evdl-extract-link-vimeo (substring url
												  (match-end 0) nil))))
	;; google video
	((string-match "video.google" url)
	 (funcall f
			  (evdl-extract-link-generic
			   url
			   "http://[A-Za-z0-9-_\.]+/videodownload"
			   "\">this link</a> and choose \"Save As\"")))
	;; guba
	((string-match "guba.com" url)
	 (funcall f
			  (concat
			   url
			   (evdl-extract-link-generic url
										  "http://free.guba.com/uploaditem"
										  "\.flv\""
										  "\.flv"))))
	;; blip.tv
	((string-match "blip.tv" url)
	 (funcall f
			  (evdl-extract-link-generic url
										 "PrimaryMediaUrl(\""
										 "\.flv.referrer"
										 "\.flv")))
	(t (message "Sorry, this url is not supported."))))

;;;;----------------------------------------------------------------------------
;;;; Support Functions
;;;;----------------------------------------------------------------------------
(defun evdl-extract-link-generic (url start-regex end-regex
								  &optional appended-string)
  "Extracts the download link from the supplied url using the
  regular expressions passed to it.  Optionally also appends an
  additional string."
  (with-temp-buffer
	(call-process wget-command nil t t
				  "--quiet"
				  "-O-"
				  url)
	(goto-char (point-min))
	(concat (substring (substring (buffer-string)
								  (string-match start-regex
												(buffer-string))
								  (string-match end-regex
												(buffer-string)))
					   (length start-regex))
			appended-string)))

;;;; Specific cases
(defun evdl-extract-link-vimeo (clipid)
  "Download a video from vimeo from the clipid."
  (with-temp-buffer
	(call-process wget-command nil t t
				  "--quiet"
				  "-O-"
				  (concat "http://www.vimeo.com/moogaloop/load/clip:"
						  clipid))
	(goto-char (point-min))
	;; remove garbage at beginning
	(delete-region (point-min)
				   (re-search-forward "<request_signature>"))
	;; build up url by prepending host and path
	(move-beginning-of-line nil)
	(insert (concat "http://www.vimeo.com/moogaloop/play/clip:"
					clipid
					"/"))
	;; tidy appended request_signature
	(move-end-of-line nil)
	(backward-delete-char (length "</request_signature>"))
	;; append request_signature_expires
	(insert "/")
	(delete-region (point)
				   (re-search-forward "<request_signature_expires"))
	(delete-region (point)
				   (re-search-forward "1"))
	(insert "1")
	(move-end-of-line nil)
	(backward-delete-char (length "</request_signature_expires>"))
	;; second line = value of request_signature_expires
	(delete-region (point)
				   (goto-char (point-max)))
	(goto-char (point-max))
	(insert "/\?q=sd")
	(buffer-string)))


(defun evdl-extract-link-youtube (videoid)
  "Download a video from youtube when given a video id and
  optionally an mp4 flag."
  (with-temp-buffer
	(call-process wget-command nil t t
				  "--quiet"
				  "-O-"
				  (concat "http://www.youtube.com/watch?v="
						  videoid
						  "&fmt=18"))
	(call-process wget-command nil t t
				  "--quiet"
				  "-O-"
				  (concat "http://www.youtube.com/watch?v="
						  videoid))
	(goto-char (point-min))
	;; remove garbage at beginning
	(delete-region (point-min)
				   (re-search-forward "\"t\": \""))
	;; remove trailing garbage
	(delete-region (re-search-forward "\"")
				   (point-max))
	(backward-delete-char 1)
	;; build up url by prepending host and path
	(move-beginning-of-line nil)
	(insert (concat "http://www.youtube.com/get_video?video_id="
					videoid
					"&t="))
	 ;; mp4 format
	(progn
	  (move-end-of-line nil)
	  (insert "&fmt=18"))
	(buffer-string)))


;;-----------------------------------------------------------------------------
;; provide package
;;-----------------------------------------------------------------------------
(provide 'evdl)